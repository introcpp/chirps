#include <iostream>
#include <stdint.h>

/*
The common field cricket chirps in direct proportion 
to the current temperature. Adding 40 to the number 
of time a cricket chirps in a minute, then dividing 
that value by 4 gives us the temperature. 
*/

// Write a program that takes as input the number of 
// chirps in a minute and prints the current temperature.

int32_t main(int32_t argc, char**argv)
{
	float chirps = 0.0F;
	float temperature = 0.0F;
	std::cout << "Input chirps per minute: ";
	std::cin >> chirps;

	// do the computation here

	std::cout << "Temperature is : " << temperature << std::endl;
}
